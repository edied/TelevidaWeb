(function ()
{
    'use strict';

    angular
            .module('fuse')
            .factory('api', apiService);

    /** @ngInject */
    function apiService($resource)
    {

        var api = {};

        // Base Url
        api.baseUrl = 'http://localhost:8080/TelevidaDevTestWS/api/';
        api.baseLocal = 'app/data/';
        api.Phone = {
            list: $resource(api.baseUrl + 'Phone/'),
            getById: $resource(api.baseUrl + 'Phone/:id', {id: '@id'})
        };
        api.PhoneRecharge = $resource(api.baseUrl + 'PhoneRecharge/:id',
                {id: '@id'},
                {
                    update: {
                        method: 'PUT' // To send the HTTP Put request when calling this custom update method.
                    }
                });
        api.CallDetailRecord = $resource(api.baseUrl + 'CallDetailRecord/:id',
                {id: '@id'},
                {
                    update: {
                        method: 'PUT' // To send the HTTP Put request when calling this custom update method.
                    }
                });
        api.TypeCall = {
            list: $resource(api.baseUrl + 'TypeCall/')
        };
        api.Country = {
            list: $resource(api.baseUrl + 'Country/')
        };
        api.Currency = {
            list: $resource(api.baseUrl + 'Currency/')
        };

        return api;
    }

})();