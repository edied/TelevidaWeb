(function ()
{
    'use strict';

    angular
            .module('app.Dashboard', [])
            .config(config);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msApiProvider, msNavigationServiceProvider)
    {
        // State
        $stateProvider
                .state('app.Dashboard', {
                    url: '/Dashboard',
                    views: {
                        'content@app': {
                            templateUrl: 'app/main/Dashboard/Dashboard.html',
                            controller: 'DashboardController as vm'
                        }
                    },
                    bodyClass: 'Dashboard'
                });

        // Translation
        $translatePartialLoaderProvider.addPart('app/main/Dashboard');

        msNavigationServiceProvider.saveItem('wr.Dashboard', {
            title: 'Dashboard',
            icon: 'icon-view-dashboard',
            state: 'app.Dashboard',
            translate: 'DASHBOARD.DASHBOARD_NAV',
            weight: 1
        });
    }
})();