(function ()
{
    'use strict';

    angular
            .module('app.Dashboard')
            .controller('DashboardController', DashboardController);

    /** @ngInject */
    function DashboardController($rootScope, api, $mdToast)
    {
        var vm = this;
        var date = new Date();
        vm.callDetailRecord = {startDate: date, endDate: date, seconds: 0, from: {id: 0}, to: {id: 0}, typeCall: {id: 0}, cost: 0.0};
        vm.phoneRecharge = {phone: {id: 0}, val: 0.0, currency: {id: 0}};
        vm.typeCalls = [];
        vm.countrys = [];
        vm.currencys = [];
        vm.phones = [];

        api.TypeCall.list.query().$promise.then(function (response) {
            console.log(response);
            vm.typeCalls = response;
        }, function (errResponse) {
            console.log(errResponse);
        });

        api.Country.list.query().$promise.then(function (response) {
            console.log(response);
            vm.countrys = response;
        }, function (errResponse) {
            console.log(errResponse);
        });

        api.Currency.list.query().$promise.then(function (response) {
            console.log(response);
            vm.currencys = response;
        }, function (errResponse) {
            console.log(errResponse);
        });

        api.Phone.list.query().$promise.then(function (response) {
            console.log(response);
            vm.phones = response;
        }, function (errResponse) {
            console.log(errResponse);
        });

        vm.timerRunning = false;

        vm.startTimer = function () {
            vm.callDetailRecord.startDate = new Date().getTime();
            $rootScope.$broadcast('timer-start');
            vm.timerRunning = true;
        };

        vm.stopTimer = function () {
            vm.callDetailRecord.endDate = new Date().getTime();
            $rootScope.$broadcast('timer-stop');
            vm.timerRunning = false;
        };
        var message = '';
        vm.saveCallDetailRecord = function () {
            vm.stopTimer();
            vm.callDetailRecord.seconds = (vm.callDetailRecord.endDate - vm.callDetailRecord.startDate) / 1000;
            console.log(vm.callDetailRecord);
            api.CallDetailRecord.save(vm.callDetailRecord).$promise.then(function (CallDetailRecord) {
                console.log(CallDetailRecord);
                vm.callDetailRecord = {startDate: date, endDate: date, seconds: 0, from: {id: 0}, to: {id: 0}, typeCall: {id: 0}, cost: 0.0};
                message = 'Llamada finalizada Exitosamente!!!';
                $mdToast.show({
                    template: '<md-toast id="language-message" layout="column" layout-align="center start"><div class="md-toast-content">' + message + '</div></md-toast>',
                    hideDelay: 7000,
                    position: 'top right',
                    parent: '#content'
                });
            }, function (errResponse) {
                console.log(errResponse);
                message = errResponse;
                $mdToast.show({
                    template: '<md-toast id="language-message" layout="column" layout-align="center start"><div class="md-toast-content">' + message + '</div></md-toast>',
                    hideDelay: 7000,
                    position: 'top right',
                    parent: '#content'
                });
            });
        };

        vm.savePhoneRecharge = function () {
            console.log(vm.phoneRecharge);
            api.PhoneRecharge.save(vm.phoneRecharge).$promise.then(function (PhoneRecharge) {
                console.log(PhoneRecharge);
                vm.phoneRecharge = {phone: {id: 0}, val: 0.0, currency: {id: 0}};
                message = 'Recarga realizada exitosamente!!!';
                $mdToast.show({
                    template: '<md-toast id="language-message" layout="column" layout-align="center start"><div class="md-toast-content">' + message + '</div></md-toast>',
                    hideDelay: 7000,
                    position: 'top right',
                    parent: '#content'
                });
            }, function (errResponse) {
                console.log(errResponse);
                message = errResponse;
                $mdToast.show({
                    template: '<md-toast id="language-message" layout="column" layout-align="center start"><div class="md-toast-content">' + message + '</div></md-toast>',
                    hideDelay: 7000,
                    position: 'top right',
                    parent: '#content'
                });
            });
        };

    }


})();
