(function ()
{
    'use strict';

    angular
            .module('app.Reports', ['flow'])
            .config(config);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider, msApiProvider, msNavigationServiceProvider)
    {

        // State
        $stateProvider
                .state('app.Reports', {
                    abstract: true,
                    url: '/Reports'
                })
                .state('app.Reports.PhoneRecharge', {
                    url: '/PhoneRecharge',
                    views: {
                        'content@app': {
                            templateUrl: 'app/main/Reports/PhoneRecharge/PhoneRecharge.html',
                            controller: 'PhoneRechargeController as vm'
                        }
                    },
                    bodyClass: 'PhoneRecharge'
                })
                .state('app.Reports.CallDetailRecord', {
                    url: '/CallDetailRecord',
                    views: {
                        'content@app': {
                            templateUrl: 'app/main/Reports/CallDetailRecord/CallDetailRecord.html',
                            controller: 'CallDetailRecordController as vm'
                        }
                    },
                    bodyClass: 'CallDetailRecord'
                })
                .state('app.Reports.Phone', {
                    url: '/Phone',
                    views: {
                        'content@app': {
                            templateUrl: 'app/main/Reports/Phone/Phone.html',
                            controller: 'PhoneController as vm'
                        }
                    },
                    bodyClass: 'Phone'
                });

        // Translation
        $translatePartialLoaderProvider.addPart('app/main/Reports');

        // Navigation
        msNavigationServiceProvider.saveItem('wr', {
            title: 'MENÚ PRINCIPAL',
            group: true,
            weight: 1
        });

        msNavigationServiceProvider.saveItem('wr.reports', {
            title: 'Reports',
            translate: 'REPORTS.TITLE',
            icon: 'icon-folder-multiple',
            weight: 1
        });
        msNavigationServiceProvider.saveItem('wr.reports.phone', {
            title: 'Phone',
            translate: 'REPORTS.PHONE_NAV',
            icon     : 'icon-phone',
            state: 'app.Reports.Phone'
        });

        msNavigationServiceProvider.saveItem('wr.reports.phoneRecharge', {
            title: 'PhoneRecharge',
            translate: 'REPORTS.PHONE_RECHARGE_NAV',
            icon     : 'icon-cash-multiple',
            state: 'app.Reports.PhoneRecharge'
        });

        msNavigationServiceProvider.saveItem('wr.reports.callDetailRecord', {
            title: 'CallDetailRecord',
            translate: 'REPORTS.CDR_NAV',
            icon     : 'icon-phone-outgoing',
            state: 'app.Reports.CallDetailRecord'
        });
    }

})();