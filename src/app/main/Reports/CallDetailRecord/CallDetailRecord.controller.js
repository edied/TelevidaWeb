(function ()
{
    'use strict';

    angular
            .module('app.Reports')
            .controller('CallDetailRecordController', CallDetailRecordController);

    /** @ngInject */
    function CallDetailRecordController($state, api)
    {
        var vm = this;

        // Data
        vm.callDetailRecords = [];
        
        api.CallDetailRecord.query().$promise.then(function (response) {
            console.log(response);
            vm.callDetailRecords = response;
        }, function (errResponse) {
            console.log(errResponse);
        });

        
        
        vm.dtInstance = {};
        vm.dtOptions = {
            dom: 'rt<"bottom"<"left"<"length"l>><"right"<"info"i><"pagination"p>>>',
            columnDefs: [],
            initComplete: function ()
            {
                var api = this.api(),
                        searchBox = angular.element('body').find('#call-detail-record-search');

                // Bind an external input as a table wide search box
                if (searchBox.length > 0)
                {
                    searchBox.on('keyup', function (event)
                    {
                        api.search(event.target.value).draw();
                    });
                }
            },
            pagingType: 'simple',
            lengthMenu: [10, 20, 30, 50, 100],
            pageLength: 20,
            scrollY: 'auto',
            responsive: true
        };


    }
})();