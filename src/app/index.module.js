(function ()
{
    'use strict';

    /**
     * Main module of the Fuse
     */
    angular
        .module('fuse', [

            // Core
            'app.core',
            
            // Satellizer
            'satellizer',

            // Navigation
            'app.navigation',

            // Toolbar
            'app.toolbar',

            // Quick panel
            'app.quick-panel',

            // Dashboard
            'app.Dashboard',
            
            // Reports
            'app.Reports'
        ]);
})();