(function ()
{
    'use strict';

    angular
            .module('fuse')
            .config(config);

    /** @ngInject */
    function config($authProvider)
    {        
        // LinkedIn
        /*$authProvider.linkedin({
            clientId: '784tqljg9ot2np'
        });*/
        
        $authProvider.oauth2({
            name: 'linkedin',
            clientId: '784tqljg9ot2np',
            url: 'https://www.linkedin.com/uas/oauth2/accessToken',
            authorizationEndpoint: 'https://www.linkedin.com/uas/oauth2/authorization',
            redirectUri: window.location.origin,
            requiredUrlParams: ['state'],
            scope: ['r_emailaddress'],
            scopeDelimiter: ' ',
            state: 'STATE',
            oauthType: '2.0',
            popupOptions: {width: 527, height: 582}
        });
    }

})();